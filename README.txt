INPUT: File having unique_urls
OUTPUT: url contents in a directory. Each url content is written in a seperate file numberd and time stamped. 


#Detailed explanation 

When executed it reads teh file 'unique_urls.txt' line by line.

Each line is treated as a URL and request is made to get contents of teh url.

Timeout is set to 5 seconds. if respone is recieved it is stored in a file.

The output is gettign dumped in a folder 'Dumped_Contents' which is created if not found in the same directory.

The output files are named File_X_TS.html where X is number of fille being written and TS is a time stamp
